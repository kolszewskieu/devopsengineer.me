---
author:
  name: "Krzysztof Olszewski"
date: 2020-07-01
draft: true
linktitle: Instalacja kubernetes za pomocą kubespray
type:
- post
- posts
title: Instalacja kubernetes za pomocą kubespray
weight: 14
series:
- Kubernetes
---


## Instalacja Kubernetes



### Infrastruktura - na czym budujemy klaster Kubernetes

Wykorzystując serwer z Proxmox na pokładzie uruchomiłem:
1. Jedna VM z 2 GB RAM, 2 rdzenie - jako kubernetes-master node
2. Trzy VM z 1.5GB RAM 2 rdzenie - jako kubernetes-nodes

Pierwszym krokiem będzie wygnerowanie kluczy aplikacji API OVH na stronie https://eu.api.ovh.com/createApp/ , na ich podsawie generujemy klucz użytkownika OVH_CONSUMER_KEY, możemy posłużyć się skryptem w pythonie z biblioteką ovh, uzupełniając odpowiednie klucze:

```
client = ovh.Client(
    endpoint='ovh-eu',
    application_key='YOUR_KEY',
    application_secret='YOUR_KEY'
)

```
https://gist.github.com/deltacodepl/df6e0c68db754ea32af5f6471a20c8e0

Umieszczamy powyższe klucze w kodzie terraform modułu 'acme_certificate'


### Terraform
#### ACME
```
resource "acme_certificate" "certificate" {
  account_key_pem           = "${acme_registration.reg.account_key_pem}"
  common_name               = "*.ecolam.pl"
  #subject_alternative_names = ["www.ecolam.pl"]

  dns_challenge {
    provider = "ovh"

    config = {
      OVH_APPLICATION_KEY     = "YOUR_KEY"
      OVH_APPLICATION_SECRET = "YOUR_KEY"
      OVH_CONSUMER_KEY = "YOUR_KEY"
      OVH_ENDPOINT = "ovh-eu"
    }
  }
}

```

#### S3
Deklarujemy 3 buckety

site-bucket - nazwa domeny, dla zawartości strony www, z dostępem "Public"

```
resource "aws_s3_bucket" "site-bucket" {
  bucket = var.domain_name
  acl    = "public-read"

  website {
    index_document = "index.html"
    error_document = "404.html"
  }

  logging {
    target_bucket = aws_s3_bucket.site-logs.bucket
    target_prefix = "${var.domain_name}/s3/logs"
  }
}
```
staging-site-bucket - dla roboczej wersji strony www, z dostępem "Public"

```
resource "aws_s3_bucket" "staging-site-bucket" {
  bucket = "review.${var.domain_name}"
  acl = "public-read"
  website {
    index_document = "index.html"
    error_document = "404.html"
  }

  logging {
    target_bucket = aws_s3_bucket.site-logs.bucket
    target_prefix = "review.${var.domain_name}/s3/logs"
  }
}
```
site-logs - miejsce na logi dla naszych stron www

```
resource "aws_s3_bucket" "site-logs" {
  bucket = var.logs_bucket
  acl    = "log-delivery-write"
}

```
### Cloudfront dla głównego bucket'a

```

# zmienna z nazwą - id bucketu
locals {
  s3_origin_id = "s3-web-${aws_s3_bucket.site-bucket.id}"
}

resource "aws_cloudfront_distribution" "site" {
  origin {
    origin_id   = local.s3_origin_id
    domain_name = aws_s3_bucket.site-bucket.website_endpoint

    # ustawienia dla bucket z zawartością statycznej strony www
    custom_origin_config {
      http_port              = "80"
      https_port             = "443"
      origin_protocol_policy = "http-only"
      origin_ssl_protocols   = ["TLSv1.2"]
    }
  }

  enabled         = true
  is_ipv6_enabled = true
  price_class     = "PriceClass_100"
  
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.s3_origin_id

    viewer_protocol_policy = "redirect-to-https"
    compress               = true

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn = "${aws_acm_certificate.cert.arn}"
    ssl_support_method  = "sni-only"
  }

  logging_config {
    include_cookies = false
    bucket          = aws_s3_bucket.site-logs.bucket_domain_name
    prefix          = "${var.domain_name}/cf/"
  }
}
```

Pełen kod infrastruktury znajdziesz tutaj:
https://github.com/deltacodepl/hugo-aws-terraform.git

### Obraz docker'a z instalacją hugo

Stworzymy obraz dockerowy z instalacją Hugo, do użytku lokalnego oraz w GitLab CI/CD

{{< gist deltacodepl dd43e8975fa543196b784bd9d8e0d638 >}}

Zbudujmy sobie dostępny lokalnie obraz

```docker build . -t deltacodepl/hugo:latest```

Stwórzmy nowy projekt


i możemy uruchomić wersję deweloperską naszej strony www

```docker run --rm -it -v $PWD:/src -p 1313:1313 -u 1000:1000 deltacodepl/hugo:latest hugo server -wD -d dev --bind=0.0.0.0```

### GitLab

{{< gist deltacodepl f10fc28011e4b7d4fc25298c56e8100d >}}